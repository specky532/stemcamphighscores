﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScores : MonoBehaviour {

    const string webURL = "http://dreamlo.com/lb/";
    const string privateCode = "ThcwPjl0gkiUKb9erTiaOwEPv6dA2sVkShkNgtqHaQfg";
    const string publicCode = "5d7b14b3d1041303ec9047cc";

    public Highscore[] highScoresList;
    static HighScores instance;
    DisplayHighScores highScoresDisplay;

    void Awake() {
        instance = this;
        highScoresDisplay = GetComponent<DisplayHighScores>();
        Application.runInBackground = true;
    }


    public static void DeleteHighScore(string username) {
        instance.StartCoroutine(instance.DeleteHighScoreFromWebsite(username));
    }


    public void DeleteMostScores() {
        for (int i = 0; i < highScoresList.Length; i++) {
            string currentUser = highScoresList[i].username;

            if (!currentUser.StartsWith("Highest")) {
                HighScores.DeleteHighScore(currentUser);
            }
        }
    }


    public void DeleteAllScores() {
        StartCoroutine(DeleteAllHighScores());
    }

    IEnumerator DeleteAllHighScores() {
        WWW www = new WWW(webURL + privateCode + "/clear/");
        yield return www;

        if (string.IsNullOrEmpty(www.error)) {
            print("Clear Scores Succesfful");
        }
        else {
            print("Error Deleting All Scores" + www.error);
        }
    }

    IEnumerator DeleteHighScoreFromWebsite(string username) {
        WWW www = new WWW(webURL + privateCode + "/delete/" + WWW.EscapeURL(username));
        yield return www;

        if (string.IsNullOrEmpty(www.error)) {
            print("Delete Succesfful");
        }
        else {
            print("Error Deleting Score" + www.error);
        }
    }

    public static void AddNewHighscore(string username, int score) {
        instance.StartCoroutine(instance.UploadNewHighScore(username, score));
    }

    IEnumerator UploadNewHighScore(string username, int score) {
        WWW www = new WWW(webURL + privateCode + "/add/" + WWW.EscapeURL(username) + "/" + score);
        yield return www;

        if (string.IsNullOrEmpty(www.error)) {
            print("Upload Succesfful");
        }
        else {
            print("Error Uploading Score" + www.error);
        }
    }

    public void DownloadHighScores() {
        StartCoroutine("DownloadHighScoresFromDataBase");
    }

    IEnumerator DownloadHighScoresFromDataBase() {
        WWW www = new WWW(webURL + publicCode + "/pipe/");
        yield return www;

        if (string.IsNullOrEmpty(www.error)) {
            FormatHighScores(www.text);
            highScoresDisplay.OnHighscoresDownloaded(highScoresList);
        }
        else {
            print("Error Downloading Scores" + www.error);
        }
    }


    void FormatHighScores(string textStream) {
        string[] entries = textStream.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
        highScoresList = new Highscore[entries.Length];

        for (int i = 0; i < entries.Length; i++) {
            string[] entryInfo = entries[i].Split(new char[] { '|' });
            string username = entryInfo[0];
            int score = int.Parse(entryInfo[1]);
            highScoresList[i] = new Highscore(username, score);
            print(highScoresList[i].username + ": " + highScoresList[i].score);
        }
    }

    public struct Highscore {
        public string username;
        public int score;

        public Highscore(string _username, int _score) {
            username = _username;
            score = _score;
        }
    }
}
