﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayHighScores : MonoBehaviour {

    public Text[] highscoreFields;
    public Text countdownClock;
    public Text refreshRatelbl;
    HighScores highscoresManager;
    float refreshRate = 10;
    string currentHighScore = "Highest";

    void Start() {
        for (int i = 0; i < highscoreFields.Length; i++) {
            highscoreFields[i].text = i + 1 + ". Fetching...";
        }


        highscoresManager = GetComponent<HighScores>();
        StartCoroutine("RefreshHighscores");
    }


    void FixedUpdate() {
        if (Input.GetKeyUp("a")) {
            DecreaseRefreshRate();
        }
        else if (Input.GetKeyUp("d")) {
            IncreaseRefreshRate();
        } else if (Input.GetKeyUp("p")) {
            DeleteAllScores();
        } else if (Input.GetKeyUp("c")) {
            DeleteNonHighScores();
        }
        refreshRatelbl.text = "Refresh Rate: " + refreshRate.ToString();
    }

    public void IncreaseRefreshRate() {
        if (refreshRate < 60) {
            refreshRate++;
        }
    }

    public void DecreaseRefreshRate() {
        if (refreshRate > 2) {
            refreshRate--;
        }
    }


    public void DeleteAllScores() {
        highscoresManager.DeleteAllScores();
    }

    public void DeleteNonHighScores() {
        highscoresManager.DeleteMostScores();
    }



    public void OnHighscoresDownloaded(HighScores.Highscore[] highscoreList) {

        for (int i = 0; i < highscoreFields.Length; i++) {
            highscoreFields[i].text = i + 1 + ". ";
            if (i < highscoreList.Length) {
                highscoreFields[i].text += highscoreList[i].username.Replace('+', ' ') + " - " + highscoreList[i].score;
            }
        }


        //Save all time high score
        if (highscoreList.Length > 0) {
            //Check what the highest score is
            //If its the same user then do nothing
            //If its a different user then delete old high score and add new one
            if(highscoreList[0].username.StartsWith("Highest")) {
                
                currentHighScore = highscoreList[0].username.Replace('+', ' ');
                print("High score maintained: " + currentHighScore);
            } else {
                //Delete old one
                //Check for left over high scores
                for (int i = 1; i < highscoreList.Length; i++) {
                    string currentUser = highscoreList[i].username;

                    if (currentUser.StartsWith("Highest")) {
                        HighScores.DeleteHighScore(currentUser);
                    }
                }
                //Enter in new high score
                currentHighScore = "Highest (" + highscoreList[0].username.Replace('+', ' ') + ")";
                print("High score changed: " + currentHighScore);

                HighScores.AddNewHighscore(currentHighScore, highscoreList[0].score);
                //highscoreFields[0].text = "1. " + currentHighScore + " - " + highscoreList[0].score;

            }
        }

    }


    IEnumerator RefreshHighscores() {
        while (true) {
            highscoresManager.DownloadHighScores();
            int counter = (int)refreshRate;
            while (counter > 0) {
                yield return new WaitForSeconds(1);
                countdownClock.text = counter.ToString();
                counter--;
            }
        }
    }
}